<?php
/*
Plugin Name: Netto 10 dager gateway
Plugin URI: https://yzkdev.no/
Description: A payment gateway for Netto 10 dager
Version: 0.3
Author: Yasin Zamani Konari
Author URI: https://yzkdev.no/
*/

add_action( 'plugins_loaded', 'init_pay_on_delivery_gateway_class' );
function init_pay_on_delivery_gateway_class() {
    class WC_Pay_On_Delivery_Gateway extends WC_Payment_Gateway {
        public function __construct() {
            $this->id = 'pay_on_delivery';
            $this->icon = '';
            $this->has_fields = false;
            $this->method_title = __( 'Netto 10 dager', 'pay-on-delivery-gateway' );
            $this->method_description = __( 'Netto 10 dager', 'pay-on-delivery-gateway' );
            $this->init_form_fields();
            $this->init_settings();
            add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
        }
        public function init_form_fields() {
            $this->form_fields = array(
                'enabled' => array(
                    'title' => __( 'Enable/Disable', 'pay-on-delivery-gateway' ),
                    'type' => 'checkbox',
                    'label' => __( 'Enable Netto 10 dager', 'pay-on-delivery-gateway' ),
                    'default' => 'yes'
                ),
                'title' => array(
                    'title' => __( 'Title', 'pay-on-delivery-gateway' ),
                    'type' => 'text',
                    'desc_tip' => __( 'Netto 10 dager.', 'pay-on-delivery-gateway' ),
                    'default' => __( 'Netto 10 dager', 'pay-on-delivery-gateway' ),
                ),
            );
        }
        public function process_payment( $order_id ) {
            global $woocommerce;
            $order = new WC_Order( $order_id );
            $order->update_status( 'on-hold', __( 'Awaiting payment', 'pay-on-delivery-gateway' ) );
            $woocommerce->cart->empty_cart();
            return array(
                'result' => 'success',
                'redirect' => $this->get_return_url( $order )
            );
        }
    }
}
add_filter( 'woocommerce_payment_gateways', 'add_pay_on_delivery_gateway_class' );
function add_pay_on_delivery_gateway_class( $methods ) {
    $methods[] = 'WC_Pay_On_Delivery_Gateway';
    return $methods;
}

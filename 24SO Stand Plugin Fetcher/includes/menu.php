<?php
/*
ini_set('display_errors', 1); 
ini_set('display_startup_errors', 1); 
error_reporting(E_ALL);
*/


function so_plugin_menu_yzk() {
    // use maybe later add_options_page('24SO Stand Plugin', '24SO Stand Plugin', 'administrator', '24so-yzk', 'so_plugin_stand_options');
    
    add_menu_page( '24SO Stand Plugin', '24SO Stand Plugin', 'administrator', 
          '24so-yzk', 'so_plugin_stand_options', '', '8' );
    add_submenu_page( '24so-yzk', '24SO Sync Single Project', '24SO Sync Single Project', 'manage_options', 'singleproject_page_24so_yzk', 'subpage_menu_24so');
    add_submenu_page( '24so-yzk', '24SO Sync All Projects', '24SO Sync All Projects', 'manage_options', 'update_all_page_24so_yzk', 'update_all_24so');
}
add_action('admin_menu', 'so_plugin_menu_yzk');

function so_plugin_stand_options() {
    $username = get_option('24SO_Username');
    $password = get_option('24SOPassword');
    $id =       get_option('twentySOID');
    $apiKey =   get_option('24SOAPIKEY');
    $dateRange = get_option('24SO_DateRange'); 
 
    if(empty($username)) {?>
        <div class="alert alert-danger width35" sttl>Please provide your login credentials or contact your administrator.</div> 
    <?php
    } else {
        $response = new twentyfourseven_sync_stand();
        ?>
    <div class="alert alert-success width35">
        <p>24SO Response</p>
        <pre><?php print_r($response->api_response_menu_page());?></pre>
    </div>     
    <?php
    //unset($response);
    }
    ?>
    <br/>
<form name="24soDetails" id="24soDetails" method="post">
  <!-- Input 1 24SO -->
  <div class="form-outline mb-4">
    <label class="form-label" for="24SOUsername">24SO Username</label>
    <input type="24SOUsername" id="24SOUsername" name="24SOUsername" class="form-control" required value="<?php echo get_option('24SO_Username');?>"/>
  </div>

  <!-- Input 2 24SO -->
  <div class="form-outline mb-4">
    <label class="form-label" for="24SOPassword">24SO Password</label>
    <input type="password" id="24SOPassword" name="24SOPassword" class="form-control" required value="<?php echo get_option('24SOPassword');?>"/>
  </div>
  <!-- Input 3 24SO -->

  <div class="form-outline mb-4">
    <label class="form-label" for="twentySOID">24SO ID</label>
    <input type="password" id="twentySOID" name="twentySOID" class="form-control" required value="<?php echo get_option('twentySOID');?>" />
  </div>

    <!-- Input 4 24SO -->

 <div class="form-outline mb-4">
    <label class="form-label" for="24SOAPIKEY">24SO APIKEY</label>
    <input type="password" id="24SOAPIKEY" name="24SOAPIKEY" class="form-control" required value="<?php echo get_option('24SOAPIKEY');?>" />
  </div>

  <div class="form-outline mb-4">
    <label class="form-label" for="DateRange">24SO DateRange</label>
    <input type="date" name="DateRange" 
        placeholder="yyyy-dd-mm" value="<?php echo get_option('24SO_DateRange');?>"
        min="2021-01-01" max="2025-01-01" id="DateRange" >
  </div> 

  <!-- Submit button -->
  <button type="submit" name="datasubmission" class="btn btn-primary btn-block mb-4" style="width: 90px;">Save</button>
  <input type="hidden" name="action" value="update" />
</form>
<?php 
if(isset($_POST['datasubmission'])) {?>
<div class="alert alert-success"> 24SO info saved</div> 
<?php
}
?>
<?php }

 
function so_plugin_submithandler(){
    //print_r($_POST);
    update_option("24SO_Username", $_POST['24SOUsername']);
    update_option("24SOPassword", $_POST['24SOPassword']);
    update_option("twentySOID", $_POST['twentySOID']);
    update_option("24SOAPIKEY", $_POST['24SOAPIKEY']);
    update_option("24SO_DateRange", $_POST['DateRange']);

}

if(isset($_POST['datasubmission'])){
    so_plugin_submithandler();
}
?>

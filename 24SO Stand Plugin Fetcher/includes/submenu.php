<?php
function update_all_24so() {?>
<form name="updateAllSync" id="updateAllSync" method="post">
  <br/>
<button type="submit" name="updateAll" class="btn btn-primary btn-block mb-4">Sync all projects</button>
</form>
<?php
if(isset($_POST['updateAll'])){
  start(false);
}
}
function subpage_menu_24so() {?>
<br/>
<form id="standlist" action="#" method="POST">
			<label for="">Select project to sync : </label>
				<select name="project_select_sync" id="project_select_sync" required>
					<?php
					switch_to_blog(2);
					global $post;
					global $wp_query;
					global $wpdb;

					$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
					$args = array(
					'posts_per_page' => -1,
					'order'=>'ASC',
					'orderby'=>'meta_value_datetime',
					'meta_key'=>'start_date',
					'meta_type'=>'DATE',
					'post_type'=> "project",
							'meta_query' => array(
							'relation' => 'AND',
							array(
								'key'     => 'type_of_proyect',
								'value'   => array('"0"','"2"'),
								'compare' => 'LIKE',
							),
							array(
								'key'     => 'end_date',
								'value'   => date('Ymd'),
								'type'    => 'DATE',
								'compare' => '>=',
							),
						), );

					remove_all_filters('posts_orderby');
					$loop = new WP_Query($args);
					if($loop->have_posts()) {
						while ($loop->have_posts()) : $loop->the_post();
            $current_id = get_the_ID();
            $project_title = get_the_title($current_id);

            restore_current_blog();
            $stand = $wpdb->get_col("SELECT p.ID FROM {$wpdb->postmeta} pm
            LEFT JOIN {$wpdb->posts} p ON p.ID = pm.post_id
            WHERE pm.meta_key = 'project' 
            AND p.post_status = 'publish' 
            AND p.post_type = 'stands'
            AND pm.meta_value = '$current_id'");
            
            if(empty($stand)) { 
              $new_stand = array(
                'post_title'    => 'Stands distribution for ' . $project_title,
                'post_status'   => 'publish',
                'post_author'   => 1,
                'post_type' => 'stands',
                'project' => $current_id,
                );
                $stand_id = wp_insert_post($my_stand);
                update_post_meta($stand_id, 'project', $current_id);
              
            } else {
              $stand_id = $stand[0];
            }
            
            switch_to_blog(2);
						$start_date = get_field('start_date', get_the_ID());
						$end_date = get_field('end_date', get_the_ID());
						$iDateFrom=mktime(1, 0, 0, substr($start_date, 4, 2), substr($start_date, 6, 2), substr($start_date, 0, 4));
						$iDateTo=mktime(1, 0, 0, substr($end_date, 4, 2), substr($end_date, 6, 2), substr($end_date, 0, 4));
						$date_text = date_i18n('j. - ', $iDateFrom) . strtolower(date_i18n('j. F Y', $iDateTo));
						?>
							<option standid="<?php echo $stand[0];?>"  value="<?php echo get_field('project_number',get_the_ID()); ?>"><?php the_title(); ?> - <?php echo $date_text; ?></option>
						<?php
						endwhile;
					}
					restore_current_blog();
					?>
				</select><br><br>
			<button type="submit" name="submit" id="submit" value="Sync"> Sync </button>
      <input type="checkbox" id="debug" name="debug" value="debug"> Display debug data</input>
		</form>
    <br/>
    <pre>
      <div id="response" style="color:green;"></div>
    </pre>
    <script>
      var value_stand = jQuery('#project_select_sync option:selected').attr("standid");
      var projectid = jQuery("#project_select_sync").val();
      jQuery('#project_select_sync').on('change', function() {
        var value_stand = jQuery('#project_select_sync option:selected').attr("standid");
        var projectid = jQuery("#project_select_sync").val();
    });
    jQuery('#submit').on('click', function(e){
          var value_stand = jQuery('#project_select_sync option:selected').attr("standid");
          var projectid = jQuery("#project_select_sync").val();
          e.preventDefault();
          syncList(value_stand, projectid);
		});

		function syncList(standid,projectid) {
      jQuery('#response').html('');
			jQuery('#submit').html('Processing...');
      jQuery('#submit').prop('disabled', true);
      var debug_false = false;
      if(jQuery('#debug').is(":checked")) {
        debug_false = true;
      }
			jQuery.ajax({
			type: "POST",
			url: "<?php echo esc_url(get_template_directory_uri()); ?>"+"/24so_standlist/processData.php",
			data: { post_id: standid , pid: projectid, debug:debug_false }
			})
			.done(function( msg ) {
				console.log(msg);
				jQuery('#response').html(msg);
        jQuery('#submit').html('Sync');
        jQuery('#submit').prop('disabled', false);
			})
			.fail(function( msg ) {
				console.log(msg);
				jQuery('#submit').html('Error');
			});
		}
    </script>
  <?php
  }

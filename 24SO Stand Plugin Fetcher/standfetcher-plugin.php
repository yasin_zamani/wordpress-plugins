<?php
/*
Plugin Name:  24SO Stand Distribution Sync
Plugin URI:   https://www.yzkdev.no
Description:  This plugin will only work for Compass Fairs Network (Syncs Stand distribution list from 24SO to BM and CF.no)
Version:      0.3
Author:       Yasin Zamani Konari
Author URI:   https://www.yzkdev.no
License:      GPL2
License URI:  https://www.gnu.org/licenses/gpl-2.0.html
*/

defined('ABSPATH') or die('No access');
include( plugin_dir_path( __FILE__ ) . '24so_files/startSync.php'); //cron job start function
include( plugin_dir_path( __FILE__ ) . '/24so_files/main.php'); // Main class 
include( plugin_dir_path( __FILE__ ) . '/includes/submenu.php'); // Sub menu pages
include( plugin_dir_path( __FILE__ ) . '/includes/menu.php'); // Menu hook
include( plugin_dir_path( __FILE__ ) . '/cron/schedule.php'); // Cron job action creation (This will be used in Cron Events (Wp-admin/ Settings -> Cron Events)).
include( plugin_dir_path( __FILE__ ) . '/includes/endpoint.php'); // Dynamic endpoint for updating public_profession meta values. This rest endpoint is secured.

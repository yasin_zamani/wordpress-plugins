<?php
define("WEBSERVICE_PATH", "https://webservices.24sevenoffice.com/");
define("API_PATH", "https://api.24sevenoffice.com/");
define('SESSION_PREFIX', 'NO_RM');
define('LOGS_DIR_24SO', get_template_directory() . '/24so_standlist/logs/');
class twentyfourseven_sync_stand {
    private $username;
    private $password;
    private $identityid;
    private $apiKey;
    private $apiResponse;
    private $options;
    private $cookie;
    private $projectId;
    private $salesUrl;
    private $dateRange;

    public function __construct(){
        $this->username = get_option('24SO_Username');
        $this->dateRange = get_option('24SO_DateRange');
        $this->password = get_option('24SOPassword');
        $this->identityid = get_option('twentySOID');
        $this->apiKey = get_option('24SOAPIKEY');
        $this->options =  array('trace' => true, 'features' => SOAP_SINGLE_ELEMENT_ARRAYS);
        $this->cookie = $_SESSION[SESSION_PREFIX]['ASP.NET_SessionId'];
        $this->projectId = null;
        $this->salesUrl = "https://webservices.24sevenoffice.com/SalesOpp/V001/SalesOppService.asmx?wsdl";
    }

    public function login() {
        $params ["credential"]["Username"] = $this->username;
        $params ["credential"]["Password"] = $this->password;
        $params ["credential"]["IdentityId"] = $this->identityid;
        $params ["credential"]["ApplicationId"] = $this->apiKey;

        try {
            $opts = array(
                'http' => array(
                    'user_agent' => 'PHPSoapClient'
                    )
                );   
            
            $context = stream_context_create($opts);
            $wsdlUrl = API_PATH . "authenticate/v001/authenticate.asmx?wsdl";
            $soapClientOptions = array(
                'trace' => true ,
                'stream_context' => $context,
                'cache_wsdl' => WSDL_CACHE_NONE
            );

            $authentication = new SoapClient ($wsdlUrl, $soapClientOptions);
            $login = true;
            if (!empty($this->cookie))
            {
                $authentication->__setCookie("ASP.NET_SessionId", $this->cookie);
                try
                {
                    $login = !($authentication->HasSession()->HasSessionResult);
                }
                catch ( SoapFault $fault ) 
                {
                    $login = true;
                }
            }
            if($login)
            {
                $result = ($temp = $authentication->Login($params));
                //Set cookie for next API call according to documentation
                $this->cookie  = $result->LoginResult;
                $authentication->__setCookie("ASP.NET_SessionId", $this->cookie);
                // throw an error if the login is unsuccessful
                if($authentication->HasSession()->HasSessionResult == false)
                    throw new SoapFault("0", "Invalid credential information.");
            }
            $this->apiResponse = $authentication->__last_response_headers;

        }
        catch ( SoapFault $fault ) 
        {
            return $fault->getCode();
        }

        return $this->apiResponse;
    }

    public function get_api_response() {
        $this->login();
        return $this->apiResponse;
    }
    public function api_response_menu_page() {
        $this->login();
        return $this->apiResponse;
    }

    public function getStandList($pid, $bool = false) {
        try {
            if (empty($this->cookie)){
                die('no session');
                return;
            }
            
            // $params["salesOppSearch"]["projectId"] = $soid;  // Not working
            $params["salesOppSearch"]["DateChangedAfter"] = $this->dateRange;
            $params["salesOppSearch"]["ProjectId"] = $pid;

            $this->projectId = $pid;
            
            $salesoppService = new SoapClient ($this->salesUrl, $this->options );
            $salesoppService->__setCookie("ASP.NET_SessionId", $this->cookie);
            if($bool == false) {
                $salesResult = $salesoppService->GetSalesOpps($params);
            } else {
                $salesResult = $salesoppService->GetSources();
            }
            
            return $salesResult;
          } 
          catch ( SoapFault $fault) {
            return $fault->getMessage();
        }

    }
    public function getPID() {
        return $this->projectId;
    } 

    public function getPerson($pid)
    {
        try {
            if (empty($this->cookie))
            {
                die('Session not set');
                return;
            }
            
            $params ["personSearch"]["CustomerId"] = $pid;
     
            $personService = new SoapClient ("https://webservices.24sevenoffice.com/CRM/Contact/PersonService.asmx?wsdl", $this->options );
            $personService->__setCookie("ASP.NET_SessionId", $this->cookie);
            $personResult = $personService->GetPersonsDetailed($params);
            return $personResult;
          } 
          catch ( SoapFault $fault ) 
          {
            return $fault->getMessage();
          }
    }

   public function getCompanyService($ccid,$bool=false) {

        $companies = Array();
        try {
            if (empty($this->cookie))
            {
                return;
            }
            
            if (is_array($ccid)) {
                $params ["searchParams"]["CompanyIds"] = $ccid;
            } else {
                $params ["searchParams"]["CompanyId"] = $ccid;
            }
            $params ["returnProperties"] = array("OrganizationNumber", "Name", "NickName", "Type", "ExternalId", "Addresses", "MemberNo", "Relations", "Owner", "IndustryId", "Relations", "Url", "PhoneNumbers", "EmailAddresses");  // "Addresses",       

            $companyService = new SoapClient ("https://api.24sevenoffice.com/CRM/Company/V001/CompanyService.asmx?wsdl", $this->options);
            $companyService->__setCookie("ASP.NET_SessionId", $this->cookie);
            if($bool==true) {
                $companyResult = $companyService->GetIndustries();
            } else {
                $companyResult = $companyService->GetCompanies($params);
            }
            return $companyResult;

          } 
          catch ( SoapFault $fault ) 
          {
            return $fault->getMessage();
          }

          return $companies;
    }

    public function activity_array() {

        $values = get_option('24so_standlist_public_prof');
        $decoded_values = json_decode($values, true);
        return $decoded_values;
    }

    public static function write($log)
	{
		$file_path = LOGS_DIR_24SO . 'logs.txt';
		$log_file = fopen($file_path, 'a');
		file_put_contents($file_path, '[' . date('Y-m-d H:i:s'). '] ' . $log.PHP_EOL , FILE_APPEND | LOCK_EX);
		fclose($log_file);
	}
    public static function post_data($params) {
        $home_url = get_template_directory_uri();
        $url = $home_url . '/24so_standlist/processData.php';
        $options = array(
            'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => $params
            )
        );
        echo "<pre>";
        print_r(file_get_contents($url, false, stream_context_create($options)));
        echo "</pre>";
    }
    
    
}



<?php
function start($cron = false) {
  switch_to_blog(2);
  global $post;
  global $wp_query;
  global $wpdb;
  $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
  $args = array(
  'posts_per_page' => -1,
  'order'=>'ASC',
  'orderby'=>'meta_value_datetime',
  'meta_key'=>'start_date',
  'meta_type'=>'DATE',
  'post_type'=> "project",
      'meta_query' => array(
      'relation' => 'AND',
      array(
        'key'     => 'type_of_proyect',
        'value'   => '"0"',
        'compare' => 'LIKE',
      ),
      array(
        'key'     => 'end_date',
        'value'   => date('Ymd'),
        'type'    => 'DATE',
        'compare' => '>=',
      ),
    ), );

  remove_all_filters('posts_orderby');
  $loop = new WP_Query($args);
  if($loop->have_posts()) {
    while ($loop->have_posts()) : $loop->the_post();
    $current_id = get_the_ID();
    restore_current_blog();
    $stand = $wpdb->get_col("SELECT p.ID FROM {$wpdb->postmeta} pm
    LEFT JOIN {$wpdb->posts} p ON p.ID = pm.post_id
    WHERE pm.meta_key = 'project' 
    AND p.post_status = 'publish' 
    AND p.post_type = 'stands'
    AND pm.meta_value = '$current_id'");
    
    if(empty($stand)) { 
      
      $new_stand = array(
        'post_title'    => 'Stands distribution for ' . $project_title,
        'post_status'   => 'publish',
        'post_author'   => 1,
        'post_type' => 'stands',
        'project' => $current_id,
        );
        $stand_id = wp_insert_post($my_stand);
        update_post_meta($stand_id, 'project', $current_id);
      
    } else {
      $stand_id = $stand[0];
    }
 
    switch_to_blog(2);
    $pid = get_field('project_number',get_the_ID());
    if($cron == true) {
      $params = "post_id=$stand_id&pid=$pid&debug=false&cronData=1";
    } else {
      $params = "post_id=$stand_id&pid=$pid&debug=false";
    }
    
    restore_current_blog();
    twentyfourseven_sync_stand::post_data($params);
    endwhile;
  }
  restore_current_blog();
}
